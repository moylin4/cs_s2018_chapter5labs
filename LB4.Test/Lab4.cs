﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text.RegularExpressions;

namespace LB4.Test
{
    [TestClass]
    public class Lab4
    {
        #region constants

        private const string PROMPT_STR = "Please enter a test score:\n";
        private const string INVALID_STR = "Invalid score\n";
        private const string COUNT_STR = "Number of test scores: {0}\n";
        private const string SUM_STR = "Sum of test scores: {1:F1}\n";
        private const string AVG_STR = "Average test scores: {2:F1}\n";
        private const string MIN_STR = "Lowest test scores: {3:F1}\n";
        private const string MAX_STR = "Highest test scores: {4:F1}\n";

        private const string PROMPT_RGX = "(Please enter a(?: test)? score:\\s*)";
        private const string INVALID_RGX = "(Invalid(?: score)?\\s*)";
        private const string COUNT_RGX = "(Number of(?: test)? scores?:\\s*(\\S*)\\s*)";
        private const string SUM_RGX = "(Sum of(?: test)? scores?:\\s*(\\S*)\\s*)";
        private const string AVG_RGX = "(Average(?: test)? scores?:\\s*(\\S*)\\s*)";
        private const string MIN_RGX = "(Lowest(?: test)? scores?:\\s*(\\S*)\\s*)";
        private const string MAX_RGX = "(Highest(?: test)? scores?:\\s*(\\S*)\\s*)";

        #endregion
        #region utils

        private static Match GetMatch(string output, string pattern)
        {
            var regex = new Regex(
                "^" + pattern + "$",
                RegexOptions.IgnoreCase | RegexOptions.Multiline
            );
            return regex.Match(output);
        }

        private static bool IsMatch(string output, string pattern)
        {
            var regex = new Regex(
                "^" + pattern + "$",
                RegexOptions.IgnoreCase | RegexOptions.Multiline
            );
            return regex.IsMatch(output);
        }

        //private static bool IsMatch(string output, string pattern, params object[] args)
        //{
        //    var regex = new Regex(
        //        string.Format("^" + pattern + "$", args),
        //        RegexOptions.IgnoreCase | RegexOptions.Multiline
        //    );
        //    return regex.IsMatch(output);
        //}

        private void CheckPrompt(string output)
        {
            if (!IsMatch(output, PROMPT_RGX + ".*"))
            {
                Assert.Fail("Expected prompt was 'Please enter a test score:'");
            }
        }

        private string RunTest(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var reader = new StringReader(input + "\0"))
                {
                    writer.NewLine = "\n";
                    Console.SetIn(reader);
                    Console.SetOut(writer);

                    Program.Main(new string[] { });

                    string output = writer.ToString();
                    return output;
                }
            }
        }
        #endregion

        [TestMethod]
        public void LB4_AcceptsOneInteger()
        {
            string input = "50\n999\n";

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                if (!IsMatch(output, PROMPT_RGX + PROMPT_RGX + ".*"))
                {
                    Assert.Fail("Did not prompt for second number");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered");
            }
        }

        [TestMethod]
        public void LB4_AcceptsOneFloat()
        {
            string input =  "50.0\n999\n";

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                if (!IsMatch(output, PROMPT_RGX + PROMPT_RGX + ".*"))
                {
                    Assert.Fail("Did not prompt for second number");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered or did not accept floating point numbers");
            }
        }

        [TestMethod]
        public void LB4_RejectsBelowZero()
        {
            string input = "-1\n50\n999\n";
            string expectedOutput =
                PROMPT_RGX + INVALID_RGX + PROMPT_RGX + PROMPT_RGX + ".*";

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                if (!IsMatch(output, expectedOutput))
                {
                    Assert.Fail("Display 'Invalid score' when the score is below 0");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered");
            }
        }

        [TestMethod]
        public void LB4_RejectsAbove100()
        {
            string input = "101\n50\n999\n";
            string expectedOutput =
                PROMPT_RGX + INVALID_RGX + PROMPT_RGX + PROMPT_RGX + ".*";

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                if (!IsMatch(output, expectedOutput))
                {
                    Assert.Fail("Display 'Invalid score' when the score is above 100");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered");
            }
        }

        [TestMethod]
        public void LB4_StatsCorrectForOneFloat()
        {
            string input = "50.0\n999\n";
            string expectedOutput = string.Format(
                PROMPT_RGX + PROMPT_RGX + 
                COUNT_RGX + SUM_RGX + AVG_RGX + MIN_RGX + MAX_RGX,
                1, 50, 50, 50, 50, 50
            );

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                if (!IsMatch(output, expectedOutput))
                {
                    Assert.Fail("Actual Output:\n{0}\n\nExpected Output:\n{1}", output, expectedOutput);
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered or did not accept floating point numbers");
            }
        }

        [TestMethod]
        public void LB4_StatsCorrectForTwoFloatsA()
        {
            string input =
                "90.0\n70.0\n999\n";
            string expectedOutputRgx = string.Format(
                PROMPT_RGX + PROMPT_RGX + PROMPT_RGX +
                COUNT_RGX + SUM_RGX + AVG_RGX + MIN_RGX + MAX_RGX,
                2, 160, 80, 70, 90
            );
            string expectedOutputStr = string.Format(
                PROMPT_STR + PROMPT_STR + PROMPT_STR +
                COUNT_STR + SUM_STR + AVG_STR + MIN_STR + MAX_STR,
                2, 160, 80, 70, 90
            );

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                var match = GetMatch(output, expectedOutputRgx);
                if (!match.Success)
                {
                    Assert.Fail("Actual Output:\n\n{0}\nExpected Output:\n\n{1}", output, expectedOutputStr);
                }
                else
                {
                    string count = match.Groups[5].Value;
                    string sum = match.Groups[7].Value;
                    string avg = match.Groups[9].Value;
                    string min = match.Groups[11].Value;
                    string max = match.Groups[13].Value;

                    Assert.AreEqual("2", count, "Count incorrect");
                    Assert.AreEqual("160.0", sum, "Sum incorrect");
                    Assert.AreEqual("80.0", avg, "Average incorrect");
                    Assert.AreEqual("70.0", min, "Min incorrect");
                    Assert.AreEqual("90.0", max, "Max incorrect");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when 999 was entered or did not accept floating point numbers");
            }
        }

        [TestMethod]
        public void LB4_StatsCorrectForTwoFloatsB()
        {
            string input =
                "70.0\n90.0\n999\n";
            string expectedOutputRgx = string.Format(
                PROMPT_RGX + PROMPT_RGX + PROMPT_RGX +
                COUNT_RGX + SUM_RGX + AVG_RGX + MIN_RGX + MAX_RGX,
                2, 160, 80, 70, 90
            );
            string expectedOutputStr = string.Format(
                PROMPT_STR + PROMPT_STR + PROMPT_STR +
                COUNT_STR + SUM_STR + AVG_STR + MIN_STR + MAX_STR,
                2, 160, 80, 70, 90
            );

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                var match = GetMatch(output, expectedOutputRgx);
                if (!match.Success)
                {
                    Assert.Fail("Actual Output:\n\n{0}\nExpected Output:\n\n{1}", output, expectedOutputStr);
                }
                else
                {
                    string count = match.Groups[5].Value;
                    string sum = match.Groups[7].Value;
                    string avg = match.Groups[9].Value;
                    string min = match.Groups[11].Value;
                    string max = match.Groups[13].Value;

                    Assert.AreEqual("2", count, "Count incorrect");
                    Assert.AreEqual("160.0", sum, "Sum incorrect");
                    Assert.AreEqual("80.0", avg, "Average incorrect");
                    Assert.AreEqual("70.0", min, "Min incorrect");
                    Assert.AreEqual("90.0", max, "Max incorrect");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Did not accept floating point numbers");
            }
        }

        [TestMethod]
        public void LB4_StatsCorrectForInvalidScores()
        {
            string input =
                "101\n-1\n50\n999\n";
            string expectedOutputRgx = string.Format(
                PROMPT_RGX + INVALID_RGX + 
                PROMPT_RGX + INVALID_RGX +
                PROMPT_RGX +
                PROMPT_RGX +
                COUNT_RGX + SUM_RGX + AVG_RGX + MIN_RGX + MAX_RGX,
                1, 50, 50, 50, 50
            );
            string expectedOutputStr = string.Format(
                PROMPT_STR + INVALID_STR +
                PROMPT_STR + INVALID_STR +
                PROMPT_STR +
                PROMPT_STR +
                COUNT_STR + SUM_STR + AVG_STR + MIN_STR + MAX_STR,
                1, 50, 50, 50, 50
            );

            try
            {
                string output = RunTest(input);
                CheckPrompt(output);

                var match = GetMatch(output, expectedOutputRgx);
                if (!match.Success)
                {
                    Assert.Fail("Actual Output:\n\n{0}\nExpected Output:\n\n{1}", output, expectedOutputStr);
                }
                else
                {
                    string count = match.Groups[8].Value;
                    string sum = match.Groups[10].Value;
                    string avg = match.Groups[12].Value;
                    string min = match.Groups[14].Value;
                    string max = match.Groups[16].Value;

                    Assert.AreEqual("1", count, "Count incorrect");
                    Assert.AreEqual("50.0", sum, "Sum incorrect");
                    Assert.AreEqual("50.0", avg, "Average incorrect");
                    Assert.AreEqual("50.0", min, "Min incorrect");
                    Assert.AreEqual("50.0", max, "Max incorrect");
                }
            }
            catch (FormatException)
            {
                Assert.Fail("Did not accept floating point numbers");
            }
        }
    }
}
