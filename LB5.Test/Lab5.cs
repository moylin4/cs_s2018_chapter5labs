﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;

namespace LB5.Test
{
    [TestClass]
    public class Lab5
    {
        #region constants

        private const string PROMPT_STR = "Please enter a test score:";
        private const string INVALID_STR = "Invalid score";
        private const string COUNT_STR = "Number of test scores: {0}";
        private const string SUM_STR = "Sum of test scores: {0:F1}";
        private const string AVG_STR = "Average test score: {0:F1}";
        private const string MIN_STR = "Lowest test score: {0:F1}";
        private const string MAX_STR = "Highest test score: {0:F1}";

        #endregion

        [TestMethod]
        public void LB5_Initialized()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();

                Assert.AreEqual("", form.lblError.Text, "lblError not blank");
                Assert.AreEqual("", form.lblCount.Text, "lblCount not blank");
                Assert.AreEqual("", form.lblSum.Text, "lblSum not blank");
                Assert.AreEqual("", form.lblAverage.Text, "lblAverage not blank");
                Assert.AreEqual("", form.lblMin.Text, "lblMin not blank");
                Assert.AreEqual("", form.lblMax.Text, "lblMax not blank");
            }
        }

        [TestMethod]
        public void LB5_AcceptsOneInteger()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "50";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreNotEqual("", form.lblCount.Text, "count not displayed");

                Assert.AreEqual(true, form.lblCount.Visible, "count not displayed");
            }
        }

        [TestMethod]
        public void LB5_AcceptsOneFloat()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "50.0";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreNotEqual("", form.lblCount.Text, "count not displayed");

                Assert.AreEqual(true, form.lblCount.Visible, "count not displayed");
            }
        }

        [TestMethod]
        public void LB5_RejectsBelowZero()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "-1";
                form.btnEnter.PerformClick();

                Assert.AreEqual(INVALID_STR, form.lblError.Text, "error not shown");
                Assert.AreEqual("", form.lblCount.Text, "count displayed");

                Assert.AreEqual(true, form.lblError.Visible, "error not shown");
            }
        }

        [TestMethod]
        public void LB5_RejectsAbove100()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "101";
                form.btnEnter.PerformClick();

                Assert.AreEqual(INVALID_STR, form.lblError.Text, "error not shown");
                Assert.AreEqual("", form.lblCount.Text, "count displayed");

                Assert.AreEqual(true, form.lblError.Visible, "error not shown");
            }
        }

        [TestMethod]
        public void LB5_StatsCorrectForOneFloat()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "50.0";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreEqual(string.Format(COUNT_STR, 1), form.lblCount.Text);
                Assert.AreEqual(string.Format(SUM_STR, 50), form.lblSum.Text);
                Assert.AreEqual(string.Format(AVG_STR, 50), form.lblAverage.Text);
                Assert.AreEqual(string.Format(MIN_STR, 50), form.lblMin.Text);
                Assert.AreEqual(string.Format(MAX_STR, 50), form.lblMax.Text);
            }
        }

        [TestMethod]
        public void LB5_StatsCorrectForTwoFloatsA()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "70.0";
                form.btnEnter.PerformClick();
                form.txtScore.Text = "90.0";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreEqual(string.Format(COUNT_STR, 2), form.lblCount.Text);
                Assert.AreEqual(string.Format(SUM_STR, 160), form.lblSum.Text);
                Assert.AreEqual(string.Format(AVG_STR, 80), form.lblAverage.Text);
                Assert.AreEqual(string.Format(MIN_STR, 70), form.lblMin.Text);
                Assert.AreEqual(string.Format(MAX_STR, 90), form.lblMax.Text);
            }
        }

        [TestMethod]
        public void LB5_StatsCorrectForTwoFloatsB()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "90.0";
                form.btnEnter.PerformClick();
                form.txtScore.Text = "70.0";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreEqual(string.Format(COUNT_STR, 2), form.lblCount.Text);
                Assert.AreEqual(string.Format(SUM_STR, 160), form.lblSum.Text);
                Assert.AreEqual(string.Format(AVG_STR, 80), form.lblAverage.Text);
                Assert.AreEqual(string.Format(MIN_STR, 70), form.lblMin.Text);
                Assert.AreEqual(string.Format(MAX_STR, 90), form.lblMax.Text);
            }
        }

        [TestMethod]
        public void LB5_StatsCorrectForInvalidScores()
        {
            using (var form = new TestScoresForm())
            {
                form.Show();
                form.txtScore.Text = "101";
                form.btnEnter.PerformClick();
                form.txtScore.Text = "-1";
                form.btnEnter.PerformClick();
                form.txtScore.Text = "50";
                form.btnEnter.PerformClick();

                Assert.AreEqual("", form.lblError.Text, "error displayed");
                Assert.AreEqual(string.Format(COUNT_STR, 1), form.lblCount.Text);
                Assert.AreEqual(string.Format(SUM_STR, 50), form.lblSum.Text);
                Assert.AreEqual(string.Format(AVG_STR, 50), form.lblAverage.Text);
                Assert.AreEqual(string.Format(MIN_STR, 50), form.lblMin.Text);
                Assert.AreEqual(string.Format(MAX_STR, 50), form.lblMax.Text);
            }
        }
    }
}
