﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using System.IO;

namespace LB6.Test
{
    [TestClass]
    public class Lab6
    {
        #region constants

        private const string PROMPT_INITIAL_STR = "Who made the sale?\n";
        private const string PROMPT_AMOUNT_STR = "How much was the sale?\n";
        private const string INVALID_INITIAL_STR = "Invalid initial\n";
        private const string INVALID_AMOUNT_STR = "Invalid amount\n";
        private const string OUTPUT_STR = 
            "Danielle Sales: {0:C}\n" +
            "Edward Sales: {1:C}\n" +
            "Francis Sales: {2:C}\n\n" +
            "Grand Total: {3:C}\n" +
            "{4} has the most sales\n";

        private const string PROMPT_INITIAL_RGX = @"(Who made the sale\?\s*)";
        private const string PROMPT_AMOUNT_RGX = @"(How much was the sale\?\s*)";
        private const string INVALID_INITIAL_RGX = @"(Invalid initial\s*)";
        private const string INVALID_AMOUNT_RGX = @"(Invalid amount\s*)";
        private const string OUTPUT_RGX =
            @"(Danielle Sales:\s*(\S*)\s*)" +
            @"(Edward Sales:\s*(\S*)\s*)" +
            @"(Francis Sales:\s*(\S*)\s*)" +
            @"(Grand Total:\s*(\S*)\s*)" +
            @"((\S*)\s*has the most sales\s*)";

        #endregion
        #region utils

        private static Match GetMatch(string text, string pattern)
        {
            var regex = new Regex(
                "^" + pattern + "$",
                RegexOptions.IgnoreCase | RegexOptions.Multiline
            );
            return regex.Match(text);
        }

        private static bool IsMatch(string text, string pattern)
        {
            var regex = new Regex(
                "^" + pattern + "$",
                RegexOptions.IgnoreCase | RegexOptions.Multiline
            );
            return regex.IsMatch(text);
        }

        //private static bool IsMatch(string output, string pattern, params object[] args)
        //{
        //    var regex = new Regex(
        //        string.Format("^" + pattern + "$", args),
        //        RegexOptions.IgnoreCase | RegexOptions.Multiline
        //    );
        //    return regex.IsMatch(output);
        //}

        private void CheckPrompt(string output)
        {
            if (!IsMatch(output, PROMPT_INITIAL_RGX + ".*"))
            {
                Assert.Fail("Expected prompt was 'Who made the sale?'");
            }
            if (!IsMatch(output, PROMPT_INITIAL_RGX + PROMPT_AMOUNT_RGX + ".*"))
            {
                Assert.Fail("Expected prompt was 'How much was the sale?'");
            }
        }

        private void CheckSales(
            string output, 
            double danielleSalesExpected, 
            double edwardSalesExpected,
            double francisSalesExpected,
            double totalSalesExpected,
            string mostSalesNameExpected)
        {
            string expectedOutputRgx = ".*" + string.Format(
                OUTPUT_RGX,
                danielleSalesExpected,
                edwardSalesExpected,
                francisSalesExpected,
                totalSalesExpected,
                mostSalesNameExpected
            );
            string expectedOutputStr = string.Format(
                OUTPUT_STR,
                danielleSalesExpected,
                edwardSalesExpected,
                francisSalesExpected,
                totalSalesExpected,
                mostSalesNameExpected
            );

            var match = GetMatch(output, expectedOutputRgx);
            if (!match.Success)
            {
                Assert.Fail("Actual Output:\n\n{0}\nExpected Output:\n\n{1}", output, expectedOutputStr);
            }
            else
            {
                string danielleSales = match.Groups[2].Value;
                string edwardSales = match.Groups[4].Value;
                string francisSales = match.Groups[6].Value;
                string totalSales = match.Groups[8].Value;
                string mostSalesName = match.Groups[10].Value;

                Assert.AreEqual(danielleSalesExpected.ToString("C"), danielleSales, "danielle sales incorrect");
                Assert.AreEqual(edwardSalesExpected.ToString("C"), edwardSales, "edward sales incorrect");
                Assert.AreEqual(francisSalesExpected.ToString("C"), francisSales, "francis sales incorrect");
                Assert.AreEqual(totalSalesExpected.ToString("C"), totalSales, "grand total incorrect");
                Assert.AreEqual(mostSalesNameExpected, mostSalesName, "most sales incorrect");
            }
        }

        private string RunTest(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var reader = new StringReader(input + "\0"))
                {
                    writer.NewLine = "\n";
                    Console.SetIn(reader);
                    Console.SetOut(writer);

                    Program.Main(new string[] { });

                    string output = writer.ToString();
                    return output;
                }
            }
        }
        #endregion

        [TestMethod]
        public void no_sales()
        {
            string input = "z\n";
            try
            {
                string output = RunTest(input);
                CheckSales(output, 0, 0, 0, 0, "Nobody");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void invalid_initial()
        {
            string input = "y\nw\nz\n";
            string expectedOutputRgx =
                PROMPT_INITIAL_RGX + INVALID_INITIAL_RGX +
                PROMPT_INITIAL_RGX + INVALID_INITIAL_RGX +
                PROMPT_INITIAL_RGX + 
                ".*";
                //OUTPUT_RGX;
            string expectedOutputStr =
                PROMPT_INITIAL_STR + INVALID_INITIAL_STR +
                PROMPT_INITIAL_STR + INVALID_INITIAL_STR +
                PROMPT_INITIAL_STR;
                //string.Format(OUTPUT_STR, 0, 0, 0, 0, "Nobody");
            
            try
            {
                string output = RunTest(input);

                if (!IsMatch(output, PROMPT_INITIAL_RGX + ".*"))
                {
                    Assert.Fail("Expected prompt was 'Who made the sale?'");
                }
                if (!IsMatch(output, expectedOutputRgx))
                {
                    Assert.Fail("Display 'Invalid initial' when the initial is invalid");
                    //Assert.Fail("Actual Output:\n\n{0}\nExpected Output:\n\n{1}", output, expectedOutputStr);
                }

                CheckSales(output, 0, 0, 0, 0, "Nobody");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void danielle_one_sale()
        {
            string input = "d\n1000\nz\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 0, 0, 1000, "Danielle");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void edward_one_sale()
        {
            string input = "e\n1000\nz\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 0, 1000, 0, 1000, "Edward");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void francis_one_sale()
        {
            string input = "f\n1000\nz\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 0, 0, 1000, 1000, "Francis");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void danielle_1000_edward_1000()
        {
            string input = 
                "d\n1000\n" + 
                "e\n1000\n" + 
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 1000, 0, 2000, "Danielle");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void danielle_1000_edward_2000()
        {
            string input =
                "d\n1000\n" +
                "e\n2000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 2000, 0, 3000, "Edward");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void edward_1000_francis_1000()
        {
            string input =
                "e\n1000\n" +
                "f\n1000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 0, 1000, 1000, 2000, "Edward");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void edward_1000_francis_2000()
        {
            string input =
                "e\n1000\n" +
                "f\n2000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 0, 1000, 2000, 3000, "Francis");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void francis_1000_danielle_1000()
        {
            string input =
                "f\n1000\n" +
                "d\n1000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 0, 1000, 2000, "Francis");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void francis_1000_danielle_2000()
        {
            string input =
                "f\n1000\n" +
                "d\n2000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 2000, 0, 1000, 3000, "Danielle");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void danielle_1000_edward_2000_francis_3000()
        {
            string input =
                "d\n1000\n" +
                "e\n2000\n" +
                "f\n3000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 2000, 3000, 6000, "Francis");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }

        [TestMethod]
        public void francis_3000_edward_2000_danielle_1000()
        {
            string input =
                "f\n3000\n" +
                "e\n2000\n" +
                "d\n1000\n" +
                "z\n";
            try
            {
                string output = RunTest(input);
                CheckPrompt(output);
                CheckSales(output, 1000, 2000, 3000, 6000, "Francis");
            }
            catch (FormatException)
            {
                Assert.Fail("Failed to stop when z was entered");
            }
        }
    }
}
