﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LB7.Test
{
    [TestClass]
    public class Lab7
    {
        #region constants
        
        private const string DANIELLE_SALES_STR = "Danielle Sales: {0:C}";
        private const string EDWARD_SALES_STR = "Edward Sales: {0:C}";
        private const string FRANCIS_SALES_STR = "Francis Sales: {0:C}";
        private const string GRAND_TOTAL_STR = "Grand Total: {0:C}";
        private const string MOST_SALES_STR = "{0} has the most sales";

        #endregion

        [TestMethod]
        public void output_is_blank_initially()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();

                Assert.AreEqual("", form.lblDanielleSales.Text, "lblDanielleSales not blank");
                Assert.AreEqual("", form.lblEdwardSales.Text, "lblEdwardSales not blank");
                Assert.AreEqual("", form.lblFrancisSales.Text, "lblFrancisSales not blank");
                Assert.AreEqual("", form.lblGrandTotalSales.Text, "lblGrandTotalSales not blank");
                Assert.AreEqual("", form.lblMostSales.Text, "lblMostSales not blank");
            }
        }

        [TestMethod]
        public void danielle_one_sale()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();

                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 1000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Danielle"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void edward_one_sale()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnEdward.PerformClick();

                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 1000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 1000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Edward"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void francis_one_sale()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnFrancis.PerformClick();

                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 1000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 1000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Francis"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void danielle_1000_edward_1000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();
                form.txtValue.Text = "1000";
                form.btnEdward.PerformClick();

                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 1000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 2000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Danielle"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void danielle_1000_edward_2000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();
                form.txtValue.Text = "2000";
                form.btnEdward.PerformClick();

                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 2000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 3000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Edward"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void edward_1000_francis_1000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnEdward.PerformClick();
                form.txtValue.Text = "1000";
                form.btnFrancis.PerformClick();
                
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 1000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 1000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 2000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Edward"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void edward_1000_francis_2000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnEdward.PerformClick();
                form.txtValue.Text = "2000";
                form.btnFrancis.PerformClick();
                
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 1000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 2000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 3000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Francis"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void francis_1000_danielle_1000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnFrancis.PerformClick();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();

                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 1000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 2000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Francis"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void francis_1000_danielle_2000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnFrancis.PerformClick();
                form.txtValue.Text = "2000";
                form.btnDanielle.PerformClick();

                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 1000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 2000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 3000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Danielle"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void danielle_1000_edward_2000_francis_3000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();
                form.txtValue.Text = "2000";
                form.btnEdward.PerformClick();
                form.txtValue.Text = "3000";
                form.btnFrancis.PerformClick();

                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 2000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 3000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 6000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Francis"), form.lblMostSales.Text);
            }
        }

        [TestMethod]
        public void francis_3000_edward_2000_danielle_1000()
        {
            using (var form = new HomeSalesForm())
            {
                form.Show();
                form.txtValue.Text = "3000";
                form.btnFrancis.PerformClick();
                form.txtValue.Text = "2000";
                form.btnEdward.PerformClick();
                form.txtValue.Text = "1000";
                form.btnDanielle.PerformClick();

                Assert.AreEqual(string.Format(DANIELLE_SALES_STR, 1000), form.lblDanielleSales.Text);
                Assert.AreEqual(string.Format(EDWARD_SALES_STR, 2000), form.lblEdwardSales.Text);
                Assert.AreEqual(string.Format(FRANCIS_SALES_STR, 3000), form.lblFrancisSales.Text);
                Assert.AreEqual(string.Format(GRAND_TOTAL_STR, 6000), form.lblGrandTotalSales.Text);
                Assert.AreEqual(string.Format(MOST_SALES_STR, "Francis"), form.lblMostSales.Text);
            }
        }
    }
}
