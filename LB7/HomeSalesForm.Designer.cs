﻿namespace LB7
{
    partial class HomeSalesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblValue = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.lblWho = new System.Windows.Forms.Label();
            this.btnDanielle = new System.Windows.Forms.Button();
            this.btnEdward = new System.Windows.Forms.Button();
            this.btnFrancis = new System.Windows.Forms.Button();
            this.lblDanielleSales = new System.Windows.Forms.Label();
            this.lblEdwardSales = new System.Windows.Forms.Label();
            this.lblFrancisSales = new System.Windows.Forms.Label();
            this.lblGrandTotalSales = new System.Windows.Forms.Label();
            this.lblMostSales = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Location = new System.Drawing.Point(12, 9);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(163, 17);
            this.lblValue.TabIndex = 0;
            this.lblValue.Text = "How much was the sale?";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(15, 30);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(310, 22);
            this.txtValue.TabIndex = 1;
            // 
            // lblWho
            // 
            this.lblWho.AutoSize = true;
            this.lblWho.Location = new System.Drawing.Point(13, 59);
            this.lblWho.Name = "lblWho";
            this.lblWho.Size = new System.Drawing.Size(138, 17);
            this.lblWho.TabIndex = 2;
            this.lblWho.Text = "Who made the sale?";
            // 
            // btnDanielle
            // 
            this.btnDanielle.Location = new System.Drawing.Point(16, 90);
            this.btnDanielle.Name = "btnDanielle";
            this.btnDanielle.Size = new System.Drawing.Size(99, 29);
            this.btnDanielle.TabIndex = 3;
            this.btnDanielle.Text = "Danielle";
            this.btnDanielle.UseVisualStyleBackColor = true;
            // 
            // btnEdward
            // 
            this.btnEdward.Location = new System.Drawing.Point(121, 90);
            this.btnEdward.Name = "btnEdward";
            this.btnEdward.Size = new System.Drawing.Size(99, 29);
            this.btnEdward.TabIndex = 4;
            this.btnEdward.Text = "Edward";
            this.btnEdward.UseVisualStyleBackColor = true;
            // 
            // btnFrancis
            // 
            this.btnFrancis.Location = new System.Drawing.Point(226, 90);
            this.btnFrancis.Name = "btnFrancis";
            this.btnFrancis.Size = new System.Drawing.Size(99, 29);
            this.btnFrancis.TabIndex = 5;
            this.btnFrancis.Text = "Francis";
            this.btnFrancis.UseVisualStyleBackColor = true;
            // 
            // lblDanielleSales
            // 
            this.lblDanielleSales.AutoSize = true;
            this.lblDanielleSales.Location = new System.Drawing.Point(13, 146);
            this.lblDanielleSales.Name = "lblDanielleSales";
            this.lblDanielleSales.Size = new System.Drawing.Size(108, 17);
            this.lblDanielleSales.TabIndex = 6;
            this.lblDanielleSales.Text = "lblDanielleSales";
            // 
            // lblEdwardSales
            // 
            this.lblEdwardSales.AutoSize = true;
            this.lblEdwardSales.Location = new System.Drawing.Point(13, 172);
            this.lblEdwardSales.Name = "lblEdwardSales";
            this.lblEdwardSales.Size = new System.Drawing.Size(104, 17);
            this.lblEdwardSales.TabIndex = 7;
            this.lblEdwardSales.Text = "lblEdwardSales";
            // 
            // lblFrancisSales
            // 
            this.lblFrancisSales.AutoSize = true;
            this.lblFrancisSales.Location = new System.Drawing.Point(13, 199);
            this.lblFrancisSales.Name = "lblFrancisSales";
            this.lblFrancisSales.Size = new System.Drawing.Size(103, 17);
            this.lblFrancisSales.TabIndex = 8;
            this.lblFrancisSales.Text = "lblFrancisSales";
            // 
            // lblGrandTotalSales
            // 
            this.lblGrandTotalSales.AutoSize = true;
            this.lblGrandTotalSales.Location = new System.Drawing.Point(13, 243);
            this.lblGrandTotalSales.Name = "lblGrandTotalSales";
            this.lblGrandTotalSales.Size = new System.Drawing.Size(129, 17);
            this.lblGrandTotalSales.TabIndex = 9;
            this.lblGrandTotalSales.Text = "lblGrandTotalSales";
            // 
            // lblMostSales
            // 
            this.lblMostSales.AutoSize = true;
            this.lblMostSales.Location = new System.Drawing.Point(13, 270);
            this.lblMostSales.Name = "lblMostSales";
            this.lblMostSales.Size = new System.Drawing.Size(87, 17);
            this.lblMostSales.TabIndex = 10;
            this.lblMostSales.Text = "lblMostSales";
            // 
            // HomeSalesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 326);
            this.Controls.Add(this.lblMostSales);
            this.Controls.Add(this.lblGrandTotalSales);
            this.Controls.Add(this.lblFrancisSales);
            this.Controls.Add(this.lblEdwardSales);
            this.Controls.Add(this.lblDanielleSales);
            this.Controls.Add(this.btnFrancis);
            this.Controls.Add(this.btnEdward);
            this.Controls.Add(this.btnDanielle);
            this.Controls.Add(this.lblWho);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.lblValue);
            this.Name = "HomeSalesForm";
            this.Text = "Home Sales";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label lblValue;
        public System.Windows.Forms.TextBox txtValue;
        public System.Windows.Forms.Label lblWho;
        public System.Windows.Forms.Button btnDanielle;
        public System.Windows.Forms.Button btnEdward;
        public System.Windows.Forms.Button btnFrancis;
        public System.Windows.Forms.Label lblDanielleSales;
        public System.Windows.Forms.Label lblEdwardSales;
        public System.Windows.Forms.Label lblFrancisSales;
        public System.Windows.Forms.Label lblGrandTotalSales;
        public System.Windows.Forms.Label lblMostSales;
    }
}

